CREATE DATABASE alten

IF (EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'dbo' AND  TABLE_NAME = 'MapRoomToAmenities'))
BEGIN
	DROP TABLE MapRoomToAmenities
END
GO

IF (EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'dbo' AND  TABLE_NAME = 'MapReservationToGuest'))
BEGIN
	DROP TABLE MapReservationToGuest
END
GO

IF (EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'dbo' AND  TABLE_NAME = 'BookedRooms'))
BEGIN
	DROP TABLE BookedRooms
END
GO

--Rooms table
IF (EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'dbo' AND  TABLE_NAME = 'Rooms'))
BEGIN
	DROP TABLE Rooms
END
GO

--Amenities table
IF (EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'dbo' AND  TABLE_NAME = 'Amenities'))
BEGIN
	DROP TABLE Amenities
END
GO

IF (EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'dbo' AND  TABLE_NAME = 'Guests'))
BEGIN
	DROP TABLE Guests
END
GO

CREATE TABLE Rooms
(
	RoomID INT IDENTITY(1,1),
	Name VARCHAR(30) not null,
	Capacity INT not null,
	Beds INT not null
	CONSTRAINT PK_Rooms PRIMARY KEY (RoomID)
)
GO

CREATE TABLE Amenities
(
	AmenitiesID INT IDENTITY(1,1),
	Name VARCHAR(20) not null
	CONSTRAINT PK_Amenities PRIMARY KEY (AmenitiesID)
)
GO

--MapRoomToAmenities

CREATE TABLE MapRoomToAmenities
(
	RoomID INT not null,
	AmenitiesID INT not null,
	CONSTRAINT PK_MapRoomToAmenities PRIMARY KEY (RoomID, AmenitiesID),
	CONSTRAINT FK_MapRoomToAmenities_RoomId FOREIGN KEY (RoomID) REFERENCES Rooms(RoomID),
	CONSTRAINT FK_MapRoomToAmenities_AmenityId FOREIGN KEY (AmenitiesID) REFERENCES Amenities(AmenitiesID)
)
GO

--Booked Rooms
CREATE TABLE BookedRooms
(
	ReservationId INT IDENTITY(1,1),
	ReservationName VARCHAR(100) not null,
	Status INT not null,
	RoomID INT not null,
	StartDate DATE not null,
	EndDate DATE not null,
	CONSTRAINT PK_BookedRooms PRIMARY KEY(ReservationId),
	CONSTRAINT FK_BookedRooms FOREIGN KEY(RoomID) REFERENCES Rooms(RoomID)
)
GO

CREATE TABLE Guests
(
	GuestID INT IDENTITY(1,1),
	Document VARCHAR(20) not null,
	Name VARCHAR(30) not null,
	AgeGroup INT not null,
	CONSTRAINT PK_Guests PRIMARY KEY(GuestID)
)
GO

CREATE TABLE MapReservationToGuest
(
	ReservationId INT not null,
	GuestID int not null,
	CONSTRAINT PK_MapReservationToGuest PRIMARY KEY(ReservationId, GuestID),
	CONSTRAINT FK_MapReservationToGuest_ReservationId FOREIGN KEY (ReservationId) REFERENCES BookedRooms(ReservationId),
	CONSTRAINT FK_MapReservationToGuest_GuestID FOREIGN KEY (GuestID) REFERENCES Guests(GuestID)
)
GO