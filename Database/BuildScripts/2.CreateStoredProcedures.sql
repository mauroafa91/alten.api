--Drops!

IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'uspGetRooms') AND type IN ( N'P', N'PC' ) )
BEGIN
	drop procedure uspGetRooms
END
GO

IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'uspGetAmenitiesForRooms') AND type IN ( N'P', N'PC' ) )
BEGIN
	drop procedure uspGetAmenitiesForRooms
END
GO

IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'uspGetAllBookingDates') AND type IN ( N'P', N'PC' ) )
BEGIN
	drop procedure uspGetAllBookingDates
END
GO

IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'uspBookRoom') AND type IN ( N'P', N'PC' ) )
BEGIN
	drop procedure uspBookRoom
END
GO

IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'uspModifyBooking') AND type IN ( N'P', N'PC' ) )
BEGIN
	drop procedure uspModifyBooking
END
GO

IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'uspCancelBooking') AND type IN ( N'P', N'PC' ) )
BEGIN
	drop procedure uspCancelBooking
END
GO

IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'uspRegisterGuests') AND type IN ( N'P', N'PC' ) )
BEGIN
	drop procedure uspRegisterGuests
END
GO

IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'uspGetGuests') AND type IN ( N'P', N'PC' ) )
BEGIN
	drop procedure uspGetGuests
END
GO

IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'uspCancelReservation') AND type IN ( N'P', N'PC' ) )
BEGIN
	drop procedure uspCancelReservation
END
GO

--Creations
CREATE PROCEDURE uspGetRooms
AS
	SELECT 
		*
	FROM
		Rooms

GO

CREATE PROCEDURE uspGetAmenitiesForRooms
@RoomId INT
AS
	SELECT
		am.Name
	FROM Amenities am
	INNER JOIN MapRoomToAmenities map on map.AmenitiesID = am.AmenitiesID
	WHERE map.RoomId = @RoomId
GO

CREATE PROCEDURE uspGetAllBookingDates
AS
	SELECT
		*
	FROM 
		BookedRooms
	WHERE
		Status != 3
GO

CREATE PROCEDURE uspBookRoom
@ReservationName VARCHAR(100),
@RoomId INT,
@StartDate DATE,
@EndDate DATE,
@Adults INT = 0,
@Children INT = 0
AS
BEGIN TRY
	INSERT INTO BookedRooms 
	VALUES(@ReservationName, 1, @RoomId, @StartDate, @EndDate)

	--Return the ReserveId
	select IDENT_CURRENT('BookedRooms')
END TRY
BEGIN CATCH
	SELECT ERROR_SEVERITY() AS severity, ERROR_LINE() AS errorLine, ERROR_MESSAGE() as message
END CATCH
GO

CREATE PROCEDURE uspModifyBooking
@ReservationId		INT,
@ReservationName	VARCHAR(100),
@RoomId				INT,
@Status				INT,
@StartDate			DATE,
@EndDate			DATE,
@Adults				INT = 0,
@Children			INT = 0
AS
BEGIN TRY
	UPDATE BookedRooms
	SET 
		ReservationName = @ReservationName,
		Status = @Status,
		StartDate = @StartDate,
		EndDate = @EndDate
	WHERE
		ReservationId = @ReservationId

	select IDENT_CURRENT('BookedRooms')
END TRY
BEGIN CATCH
	SELECT ERROR_SEVERITY() AS severity, ERROR_LINE() AS errorLine, ERROR_MESSAGE() as message
END CATCH
GO

CREATE PROCEDURE uspCancelBooking
@ReservationId		INT
AS
	DELETE FROM BookedRooms WHERE ReservationId = @ReservationId
GO

CREATE PROCEDURE uspRegisterGuests
@GuestID		INT = -1,
@ReservationID	INT,
@Document		VARCHAR(20),
@Name			VARCHAR(30),
@AgeGroup		INT
AS
BEGIN TRY
	IF EXISTS (SELECT * FROM Guests WHERE GuestID = @GuestID)
	BEGIN
		UPDATE Guests
		SET Document = @Document, Name = @Name, AgeGroup = @AgeGroup
		WHERE GuestID = @GuestID
	END
	ELSE
	BEGIN
		INSERT INTO Guests
		VALUES (@Document, @Name, @AgeGroup)
		SET @GuestID = (select IDENT_CURRENT('Guests'))
	END
	
	IF NOT EXISTS (SELECT * FROM MapReservationToGuest WHERE ReservationId = @ReservationID AND GuestID = @GuestID)
	BEGIN
		INSERT INTO MapReservationToGuest
		VALUES (@ReservationID, @GuestID)
	END
END TRY
BEGIN CATCH
	SELECT ERROR_SEVERITY() AS severity, ERROR_LINE() AS errorLine, ERROR_MESSAGE() as message
END CATCH
GO

CREATE PROCEDURE uspGetGuests
AS
	SELECT 
		g.*, map.ReservationId
	FROM
		Guests g 
	INNER JOIN MapReservationToGuest map on map.GuestID = g.GuestID 
	INNER JOIN BookedRooms br ON br.ReservationId = map.ReservationId
	WHERE 
		br.Status IN (1,2)
GO

CREATE PROCEDURE uspCancelReservation
@ReservationID AS INT
AS
	UPDATE BookedRooms
	SET Status = 3
	WHERE ReservationId = @ReservationID