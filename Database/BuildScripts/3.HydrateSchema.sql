IF (EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'dbo' AND  TABLE_NAME = 'MapRoomToAmenities'))
BEGIN
	delete from MapRoomToAmenities
END
GO

IF (EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'dbo' AND  TABLE_NAME = 'Amenities'))
BEGIN
	delete from Amenities
	DBCC CHECKIDENT ('Amenities', RESEED, 1)
END
GO

--Only one room for now
IF (EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'dbo' AND  TABLE_NAME = 'Rooms'))
BEGIN
	delete from Rooms
	DBCC CHECKIDENT ('Rooms', RESEED, 1)
END	
GO
INSERT INTO Rooms VALUES
('Presidential Suite', 4, 2)
GO
--Amenities, this is just extra stuff on my head

INSERT INTO Amenities VALUES 
('Jacuzzi'),
('Air Conditioning'),
('Hot Water'),
('Mini Fridge')
GO
--Mapping
INSERT INTO MapRoomToAmenities VALUES
(1, 1),
(1, 2),
(1, 3),
(1, 4)
GO
--BookedRooms, should we hydrate what we have available here?