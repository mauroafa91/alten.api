# README #

* Summary of set up
  * Booking API made to handle more than one room from the start, it comes with 3 sql server scripts to create supporting and required database.
  * The idea of the API is that it'll only store reserved/booked dates and it expects the UI to handle the dates that are available based on that.
  * Only contains ONE controller, called "Booking Controller" which contains the following endpoints
  * Every endpoint has the prefix "api/booking", please not that some of the logic is in the database.
    * **dates**
        Gets all rooms with full information, including occupied dates so the UI can handle which ones are available
    * **book**
        Initiates booking of a room, it performs multiple validations before actually doing any database work
    * **modify**
        This method is used to either change a specific data for the reservation or by changing it from "Reserved" to "Booked, as we know, all reservations
        are just that until they are actually paid
    * **cancel**
        Cancels the reservation only by updating a record in the database to that status, records are kept for reporting reasons
    * **GetBooking**
        Gets a specific booking based on ReservationID
* Configuration
  * Execute scripts contained in the Database directory in the named order.
* Dependencies
  * Requires SQL Server scripts to be executed somewhere and connection string found in Src\Alten.API\appsettings.json under "ConnectionStrings/AltenDB"