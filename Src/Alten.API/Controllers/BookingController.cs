﻿using Alten.API.Exceptions;
using Alten.API.Repository;
using Alten.DataContracts;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Alten.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BookingController : ControllerBase
    {
        private const int MAX_DAYS = 3;
        private const int MAX_DAYS_IN_ADVANCE = 30;

        private readonly ILogger<BookingController> _logger;

        public BookingController(ILogger<BookingController> logger)
        {
            _logger = logger;
        }

        // GET: api/<BookingController>
        [HttpGet("dates")]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(List<BookRoomDto>), StatusCodes.Status200OK)]
        public IActionResult Get([FromServices] IRepository<RoomDto> roomRepository)
        {
            List<RoomDto> rooms;

            try
            {
                _logger.LogInformation("Calling get all reservations");
                //This response contains a list of rooms which inside contains a list of bookroom objects showing booked ranges of dates by reservation
                rooms = roomRepository.GetAll().ToList();
            }
            catch(Exception ex)
            {
                _logger.LogError($"Something failed retrieving all available rooms");
                return BadRequest($"Unexpect error getting available dates for rooms, error: {ex.Message}");
            }

            return Ok(rooms);
        }

        // POST api/<BookingController>
        [HttpPost("book")]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(BookRoomDto), StatusCodes.Status200OK)]
        public IActionResult Book([FromBody] BookRoomDto request, [FromServices] IRepository<BookRoomDto> bookRoomRepository)
        {
            //Need to check if days in between are available...
            try
            {
                IsBookingValid(request);
                bookRoomRepository.Add(request);
            }
            catch(BookingException bEx)
            {
                _logger.LogError(bEx, $"Booking exception: {bEx.Message}");
                return BadRequest(bEx.Message);
            }
            catch(Exception ex)
            {
                throw ex;
            }

            return Ok(request);
        }

        //This method will be used to book
        [HttpPost("modify")]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(BookRoomDto), StatusCodes.Status200OK)]
        public IActionResult Modify([FromBody] BookRoomDto request, [FromServices] IRepository<BookRoomDto> bookRoomRepository)
        {
            try
            {
                IsBookingValid(request);
                bookRoomRepository.Modify(request);
            }
            catch (BookingException bEx)
            {
                _logger.LogError(bEx, $"Booking exception: {bEx.Message}");
                return BadRequest(bEx.Message);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return Ok(request);
        }

        [HttpDelete("cancel/{ReservationId}")]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(BookRoomDto), StatusCodes.Status200OK)]
        public IActionResult Cancel(int ReservationId, [FromServices] IRepository<BookRoomDto> bookRoomRepository)
        {
            try
            {
                bookRoomRepository.Cancel(ReservationId);
            }
            catch (BookingException bEx)
            {
                _logger.LogError(bEx, $"Booking exception: {bEx.Message}");
                return BadRequest(bEx.Message);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return Ok();
        }

        [HttpGet("getbooking/{ReservationID}")]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(BookRoomDto), StatusCodes.Status200OK)]
        public IActionResult GetBooking(int ReservationID, [FromServices] IRepository<BookRoomDto> bookRoomRepository)
        {
            BookRoomDto bookRoom;
            try
            {
                bookRoom = bookRoomRepository.Get(ReservationID);
            }
            catch (BookingException bEx)
            {
                _logger.LogError(bEx, $"Booking exception: {bEx.Message}");
                return BadRequest(bEx.Message);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Ok(bookRoom);
        }
        private void IsBookingValid(BookRoomDto booking)
        {
            if ((booking.BookedDates.EndDate - booking.BookedDates.StartDate).Days > MAX_DAYS)
            {
                throw new BookingException("Cannot book a room for more than 3 days");
            }

            //Check if ending date is set correctly
            if (booking.BookedDates.EndDate < booking.BookedDates.StartDate)
            {
                throw new BookingException($"End date - {booking.BookedDates.EndDate} should be sooner than Start date: {booking.BookedDates.StartDate}");
            }

            //Start date should be the next day to the current one
            var currentDate = DateTime.Today;
            if (booking.BookedDates.StartDate <= currentDate)
            {
                throw new BookingException($"Start date - {booking.BookedDates.StartDate} must start at least the next current day");
            }

            if ((booking.BookedDates.StartDate - currentDate).Days > MAX_DAYS_IN_ADVANCE)
            {
                throw new BookingException($"Cannot book a room more than {MAX_DAYS_IN_ADVANCE} days in advance");
            }
        }
    }
}
