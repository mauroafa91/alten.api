﻿using Alten.DataContracts;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace Alten.API.Exceptions
{
    public class BookingException : Exception
    {
        public BookingException() : base() { }
        public BookingException(string message) : base(message) { }
        public BookingException(string message, params object[] args)
            : base(String.Format(CultureInfo.CurrentCulture, message, args)) { }
        public BookingException(BookRoomDto bookRoomDto) : base(String.Format(CultureInfo.CurrentCulture, bookRoomDto.ToString())) { }
    }
}
