﻿using System;
using System.Collections.Generic;

namespace Alten.API.Repository
{
    public abstract class BaseRepository<T> : IRepository<T> where T : class
    {
        public IDBWrapper DBWrapper { get; set; }

        ~BaseRepository()
        {
            Dispose();
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }

        public virtual T Get(int ID)
        {
            throw new NotImplementedException();
        }

        public virtual IEnumerable<T> GetAll()
        {
            throw new NotImplementedException();
        }

        public virtual T Add(T t)
        {
            throw new NotImplementedException();
        }

        public virtual T Modify(T t)
        {
            throw new NotImplementedException();
        }
    }
}
