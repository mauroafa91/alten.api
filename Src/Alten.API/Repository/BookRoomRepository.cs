﻿using Alten.API.Exceptions;
using Alten.DataContracts;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Alten.API.Repository
{
    public class BookRoomRepository : BaseRepository<BookRoomDto>
    {
        private IRepository<GuestDto> _guestRepository;
        public BookRoomRepository(IDBWrapper dbWrapper, IRepository<GuestDto> guestRepository)
        {
            DBWrapper = dbWrapper;
            _guestRepository = guestRepository;
        }
        public override BookRoomDto Get(int ID)
        {
            BookRoomDto booking;
            try
            {
                booking = GetAll().FirstOrDefault(x => x.ReservationId == ID);

                booking.Guests = _guestRepository.GetAll().Where(x => x.ReservationID == ID).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return booking;
        }
        public override IEnumerable<BookRoomDto> GetAll()
        {
            List<BookRoomDto> bookings;
            try
            {
                bookings = DBWrapper.GetBooking();
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return bookings;
        }
        public override BookRoomDto Add(BookRoomDto t)
        {
            try
            {
                CheckIfAlreadyBooked(t);
                //Let's just check one more time if these dates are already reserved
                DBWrapper.BookRoom(t);
                
                foreach(GuestDto guest in t.Guests)
                {
                    guest.ReservationID = t.ReservationId;
                    _guestRepository.Add(guest);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return t;
        }

        public override BookRoomDto Modify(BookRoomDto t)
        {
            try
            {
                CheckIfAlreadyBooked(t);
                DBWrapper.ModifyBooking(t);
                //Modify guests if any, and decide wether to update or add...
                foreach (GuestDto guest in t.Guests)
                {
                    _guestRepository.Add(guest);
                }
            }
            catch(Exception ex)
            {
                throw ex;
            }
            return t;
        }

        private void CheckIfAlreadyBooked(BookRoomDto booking)
        {
            IEnumerable<BookRoomDto> allBookings = GetAll();

            if (booking.ReservationId != -1 && !allBookings.Any(x => x.ReservationId == booking.ReservationId))
                throw new BookingException($"Reservation ID: {booking.ReservationId} not found");

            BookRoomDto alreadyBooked = allBookings.Where(x => x.ReservationId != booking.ReservationId).FirstOrDefault(x => (booking.BookedDates.StartDate >= x.BookedDates.StartDate && booking.BookedDates.StartDate <= x.BookedDates.EndDate)
            || (booking.BookedDates.EndDate >= x.BookedDates.StartDate && booking.BookedDates.EndDate <= x.BookedDates.EndDate));

            if (alreadyBooked != null)
            {
                //Log this here and maybe throw an error
                throw new BookingException($"There's another reservation: {alreadyBooked}");
            }
        }
    }
}
