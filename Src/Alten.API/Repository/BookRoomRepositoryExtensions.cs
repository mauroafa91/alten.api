﻿using Alten.DataContracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Alten.API.Repository
{
    public static class BookRoomRepositoryExtensions
    {
        public static void Cancel(this IRepository<BookRoomDto> repository, int reservationId)
        {
            try
            {
                repository.DBWrapper.CancelBooking(reservationId);
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
    }
}
