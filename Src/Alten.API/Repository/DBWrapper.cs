﻿using Alten.API.Exceptions;
using Alten.DataContracts;
using Alten.DataContracts.Enumerations;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace Alten.API.Repository
{
    public class DBWrapper : IDBWrapper
    {
        //Requests
        private readonly string GET_BOOK_ROOM_SP = "uspGetRooms";
        private readonly string AMENITIES_FOR_ROOM_SP = "uspGetAmenitiesForRooms";
        private readonly string BOOKED_DATES_FOR_ROOM = "uspGetAllBookingDates";
        private readonly string GET_GUESTS_SP = "uspGetGuests";

        //Actions
        private readonly string BOOK_ROOM_SP = "uspBookRoom";
        private readonly string MODIFY_BOOKING_SP = "uspModifyBooking";
        private readonly string REGISTER_GUEST_SP = "uspRegisterGuests";
        private readonly string CANCEL_BOOKING_SP = "uspCancelReservation";

        private static Lazy<IDBWrapper> _instance = null;
        private SqlConnection _sqlConnection;
        private DBWrapper(string connectionString)
        {
            _sqlConnection = new SqlConnection(connectionString);
        }

        public static void Create(string connectionString) => _instance = new Lazy<IDBWrapper>(() => new DBWrapper(connectionString));

        public static IDBWrapper Instance => _instance.Value;

        #region Interface
        public void BookRoom(BookRoomDto booking)
        {
            try
            {
                _sqlConnection.Open();
                SqlCommand command = new SqlCommand(BOOK_ROOM_SP, _sqlConnection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add(new SqlParameter("@ReservationName", booking.ReservationName));
                command.Parameters.Add(new SqlParameter("@RoomId", booking.RoomId));
                command.Parameters.Add(new SqlParameter("@StartDate", booking.BookedDates.StartDate));
                command.Parameters.Add(new SqlParameter("@EndDate", booking.BookedDates.EndDate));

                using (var dataReader = command.ExecuteReader())
                {
                    while (dataReader.Read())
                    {
                        if(dataReader.FieldCount == 1)
                        {
                            booking.ReservationId = (int)dataReader.GetDecimal(0);
                        }
                        //If this happens, then we need to retrieve errors...
                        else
                        {
                            throw new BookingException($"Severity {dataReader.GetString(0)}, ErrorLine: {dataReader.GetString(1)}, Message: {dataReader.GetString(2)}");
                        }
                    }
                }
                _sqlConnection.Close();
            }
            catch (SqlException sqlEx)
            {
                _sqlConnection.Close();
                throw sqlEx; //TODO properly log stuff here
            }
            catch (Exception ex)
            {
                _sqlConnection.Close();
                throw ex;
            }
        }

        public void ModifyBooking(BookRoomDto booking)
        {
            try
            {
                _sqlConnection.Open();
                SqlCommand command = new SqlCommand(MODIFY_BOOKING_SP, _sqlConnection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add(new SqlParameter("@ReservationId", booking.ReservationId));
                command.Parameters.Add(new SqlParameter("@ReservationName", booking.ReservationName));
                command.Parameters.Add(new SqlParameter("@RoomId", booking.RoomId));
                command.Parameters.Add(new SqlParameter("@Status", booking.Status));
                command.Parameters.Add(new SqlParameter("@StartDate", booking.BookedDates.StartDate));
                command.Parameters.Add(new SqlParameter("@EndDate", booking.BookedDates.EndDate));

                using (var dataReader = command.ExecuteReader())
                {
                    while (dataReader.Read())
                    {
                        if (dataReader.FieldCount == 1)
                        {
                            booking.ReservationId = (int)dataReader.GetDecimal(0);
                        }
                        //If this happens, then we need to retrieve errors...
                        else
                        {
                            throw new BookingException($"Severity {dataReader.GetString(0)}, ErrorLine: {dataReader.GetString(1)}, Message: {dataReader.GetString(2)}");
                        }
                    }
                }
                _sqlConnection.Close();
            }
            catch (SqlException sqlEx)
            {
                _sqlConnection.Close();
                throw sqlEx; //TODO properly log stuff here
            }
            catch (Exception ex)
            {
                _sqlConnection.Close();
                throw ex;
            }
        }

        public void RegisterGuest(GuestDto guest)
        {
            try
            {
                _sqlConnection.Open();
                SqlCommand command = new SqlCommand(REGISTER_GUEST_SP, _sqlConnection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add(new SqlParameter("@GuestID", guest.GuestID));
                command.Parameters.Add(new SqlParameter("@ReservationId", guest.ReservationID));
                command.Parameters.Add(new SqlParameter("@Document", guest.Document));
                command.Parameters.Add(new SqlParameter("@Name", guest.Name));
                command.Parameters.Add(new SqlParameter("@AgeGroup", guest.AgeGroup));

                using (var dataReader = command.ExecuteReader())
                {
                    while (dataReader.Read())
                    {
                        if (dataReader.FieldCount == 3)
                        {
                            throw new BookingException($"Severity {dataReader.GetInt32(0)}, ErrorLine: {dataReader.GetInt32(1)}, Message: {dataReader.GetString(2)}");
                        }
                    }
                }
                _sqlConnection.Close();
            }
            catch (SqlException sqlEx)
            {
                _sqlConnection.Close();
                throw sqlEx; //TODO properly log stuff here
            }
            catch (Exception ex)
            {
                _sqlConnection.Close();
                throw ex;
            }
        }

        public void CancelBooking(int BookingId)
        {
            try
            {
                _sqlConnection.Open();
                SqlCommand command = new SqlCommand(CANCEL_BOOKING_SP, _sqlConnection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add(new SqlParameter("@ReservationID", BookingId));

                command.ExecuteNonQuery();

                _sqlConnection.Close();
            }
            catch (SqlException sqlEx)
            {
                _sqlConnection.Close();
                throw sqlEx; //TODO properly log stuff here
            }
            catch (Exception ex)
            {
                _sqlConnection.Close();
                throw ex;
            }
        }

        public List<string> GetAmenities(int RoomId)
        {
            List<string> amenities = new List<string>();

            try
            {
                _sqlConnection.Open();
                SqlCommand command = new SqlCommand(AMENITIES_FOR_ROOM_SP, _sqlConnection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add(new SqlParameter("@RoomId", RoomId));
                using (var dataReader = command.ExecuteReader())
                {
                    while (dataReader.Read())
                    {
                        amenities.Add(dataReader.GetString(0));
                    }
                }
                _sqlConnection.Close();
            }
            catch(SqlException sqlEx)
            {
                _sqlConnection.Close();
                throw sqlEx; //TODO properly log stuff here
            }
            catch(Exception ex)
            {
                _sqlConnection.Close();
                throw ex;
            }
            return amenities;
        }

        public List<BookRoomDto> GetBooking()
        {
            List<BookRoomDto> bookings = new List<BookRoomDto>();

            try
            {
                _sqlConnection.Open();
                SqlCommand command = new SqlCommand(BOOKED_DATES_FOR_ROOM, _sqlConnection);

                using (var dataReader = command.ExecuteReader())
                {
                    while (dataReader.Read())
                    {
                        BookRoomDto bookRoomDto = new BookRoomDto()
                        {
                            ReservationId = dataReader.GetInt32(0),
                            ReservationName = dataReader.GetString(1),
                            Status = (BookingStatus)dataReader.GetInt32(2),
                            RoomId = dataReader.GetInt32(3),
                            BookedDates = new BookedDatesDto() 
                            { 
                                StartDate = dataReader.GetDateTime(4), 
                                EndDate = dataReader.GetDateTime(5) 
                            }
                        };
                        bookings.Add(bookRoomDto);
                    }
                }
                _sqlConnection.Close();
            }
            catch (SqlException sqlEx)
            {
                _sqlConnection.Close();
                throw sqlEx; //TODO properly log stuff here
            }
            catch (Exception ex)
            {
                _sqlConnection.Close();
                throw ex;
            }
            return bookings;
        }

        public List<RoomDto> GetRooms()
        {
            List<RoomDto> rooms = new List<RoomDto>();

            try
            {
                _sqlConnection.Open();
                using (var command = new SqlCommand(GET_BOOK_ROOM_SP, _sqlConnection))
                using (var dataReader = command.ExecuteReader())
                {
                    while (dataReader.Read())
                    {
                        RoomDto room = new RoomDto()
                        {
                            RoomId = dataReader.GetInt32(0),
                            Name = dataReader.GetString(1),
                            Capacity = dataReader.GetInt32(2),
                            Beds = dataReader.GetInt32(3)
                        };
                        rooms.Add(room);
                    }
                }
                _sqlConnection.Close();

            }
            catch (SqlException sqlEx)
            {
                _sqlConnection.Close();
                throw sqlEx; //TODO properly log stuff here
            }
            catch (Exception ex)
            {
                _sqlConnection.Close();
                throw ex;
            }

            return rooms;
        }

        public List<GuestDto> GetGuests()
        {
            List<GuestDto> guests = new List<GuestDto>();

            try
            {
                _sqlConnection.Open();
                using (var command = new SqlCommand(GET_GUESTS_SP, _sqlConnection))
                using (var dataReader = command.ExecuteReader())
                {
                    while (dataReader.Read())
                    {
                        GuestDto guest = new GuestDto()
                        {
                            GuestID = dataReader.GetInt32(0),
                            Document = dataReader.GetString(1),
                            Name = dataReader.GetString(2),
                            AgeGroup = (AgeGroup)dataReader.GetInt32(3),
                            ReservationID = dataReader.GetInt32(4)
                        };
                        guests.Add(guest);
                    }
                }
                _sqlConnection.Close();
            }
            catch (SqlException sqlEx)
            {
                _sqlConnection.Close();
                throw sqlEx; //TODO properly log stuff here
            }
            catch (Exception ex)
            {
                _sqlConnection.Close();
                throw ex;
            }

            return guests;
        }

        #endregion
    }
}
