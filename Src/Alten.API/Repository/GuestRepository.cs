﻿using Alten.DataContracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Alten.API.Repository
{
    public class GuestRepository : BaseRepository<GuestDto>
    {
        public GuestRepository(IDBWrapper dbWrapper)
        {
            DBWrapper = dbWrapper;
        }

        public override GuestDto Add(GuestDto t)
        {
            DBWrapper.RegisterGuest(t);
            return t;
        }

        public override IEnumerable<GuestDto> GetAll()
        {
            List<GuestDto> guests;
            try
            {
                guests = DBWrapper.GetGuests();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return guests;
        }
    }
}
