﻿using Alten.DataContracts;
using System;
using System.Collections.Generic;

namespace Alten.API.Repository
{
    public interface IDBWrapper
    {
        List<RoomDto> GetRooms();
        List<string> GetAmenities(int RoomId);
        List<BookRoomDto> GetBooking();
        List<GuestDto> GetGuests();

        //Actions
        void BookRoom(BookRoomDto booking);
        void ModifyBooking(BookRoomDto booking);
        void CancelBooking(int BookingId);
        void RegisterGuest(GuestDto guest);
    }
}
