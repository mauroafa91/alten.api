﻿using System;
using System.Collections.Generic;

namespace Alten.API.Repository
{
    public interface IRepository<T> : IDisposable where T : class
    {
        IEnumerable<T> GetAll();
        T Get(int ID);
        T Add(T t);
        T Modify(T t);

        IDBWrapper DBWrapper { get; set; }
    }
}
