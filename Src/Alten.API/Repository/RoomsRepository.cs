﻿using Alten.DataContracts;
using System.Collections.Generic;
using System.Linq;

namespace Alten.API.Repository
{
    public class RoomsRepository : BaseRepository<RoomDto>
    {
        private IDBWrapper _dbWrapper;
        private IRepository<BookRoomDto> _bookRoomDtoRepository;
        public RoomsRepository(IDBWrapper dbWrapper, IRepository<BookRoomDto> bookRoomDtoRepository)
        {
            _dbWrapper = dbWrapper;
            _bookRoomDtoRepository = bookRoomDtoRepository;
        }
        public override IEnumerable<RoomDto> GetAll()
        {
            List<RoomDto> rooms = _dbWrapper.GetRooms();
            
            foreach(RoomDto singleRoom in rooms)
            {
                //Get amenities
                singleRoom.Amenities = _dbWrapper.GetAmenities(singleRoom.RoomId);
                //Get booked dates
                singleRoom.BookedDates = _bookRoomDtoRepository.GetAll()
                    .ToList().Where(x => x.RoomId == singleRoom.RoomId).Select(x => new BookedDatesDto() { StartDate = x.BookedDates.StartDate, EndDate = x.BookedDates.EndDate }).ToList();
            }

            return rooms;
        }
    }
}
