using Alten.API.Repository;
using Alten.DataContracts;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace Alten.API
{
    public class Startup
    {
        private readonly string CONNECTION_STRING_KEY = "AltenDB";
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();

            string connectionString = Configuration.GetConnectionString(CONNECTION_STRING_KEY);
            DBWrapper.Create(connectionString);
            services
                .AddSingleton(typeof(IDBWrapper), DBWrapper.Instance)
                .AddScoped<IRepository<GuestDto>, GuestRepository>()
                .AddScoped<IRepository<BookRoomDto>, BookRoomRepository>()
                .AddScoped<IRepository<RoomDto>, RoomsRepository>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
