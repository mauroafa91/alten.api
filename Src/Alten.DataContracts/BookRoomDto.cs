﻿using Alten.DataContracts.Enumerations;
using System.Collections.Generic;

namespace Alten.DataContracts
{
    public class BookRoomDto
    {
        public BookRoomDto()
        {
            Status = BookingStatus.Initialized;
            ReservationId = -1;
        }

        public int ReservationId { get; set; }
        public string ReservationName { get; set; }
        public BookingStatus Status { get; set; }
        public int RoomId { get; set; }
        public BookedDatesDto BookedDates { get; set; }
        public List<GuestDto> Guests { get; set; }

        public override string ToString()
        {
            return $"ReservationID {ReservationId}, RoomID: {RoomId}, Dates: {BookedDates}";
        }
    }
}
