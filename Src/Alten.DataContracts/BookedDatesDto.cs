﻿using System;

namespace Alten.DataContracts
{
    public class BookedDatesDto
    {
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }

        public override string ToString()
        {
            return $"Start Date: {StartDate} - End Date: {EndDate}";
        }
    }
}
