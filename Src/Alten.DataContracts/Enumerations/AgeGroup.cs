﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Alten.DataContracts.Enumerations
{
    public enum AgeGroup
    {
        Baby,
        Children,
        Adult
    }
}
