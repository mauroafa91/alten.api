﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Alten.DataContracts.Enumerations
{
    public enum BookingStatus
    {
        Initialized = -1,
        Reserved,
        Booked,
        Cancelled,
        Finalized
    }
}
