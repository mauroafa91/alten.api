﻿using Alten.DataContracts.Enumerations;

namespace Alten.DataContracts
{
    public class GuestDto
    {
        public int GuestID { get; set; }
        public string Document { get; set; }
        public string Name { get; set; }
        public AgeGroup AgeGroup { get; set; }
        public int ReservationID { get; set; }
    }
}
