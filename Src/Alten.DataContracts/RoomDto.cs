﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Alten.DataContracts
{
    public class RoomDto
    {
        public int RoomId { get; set; }
        public string Name { get; set; }
        public int Capacity { get; set; }
        public int Beds { get; set; }
        public List<BookedDatesDto> BookedDates { get; set; }
        public List <string> Amenities { get; set; }
    }
}
