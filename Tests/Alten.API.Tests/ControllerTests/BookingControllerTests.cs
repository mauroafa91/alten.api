﻿using Alten.API.Controllers;
using Alten.API.Repository;
using Alten.DataContracts;
using Alten.DataContracts.Enumerations;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;

namespace Alten.API.Tests.ControllerTests
{
    class BookingControllerTests
    {
        [Test]
        public void GetAllBookedDates()
        {
            //Arrange
            var mockWrapper = new Mock<IDBWrapper>();
            mockWrapper.Setup(wrapper => wrapper.GetRooms()).Returns(TestRooms()).Verifiable();
            mockWrapper.Setup(wrapper => wrapper.GetAmenities(1)).Returns(TestAmenities()).Verifiable();
            mockWrapper.Setup(wrapper => wrapper.GetBooking()).Returns(TestBooking()).Verifiable();

            GuestRepository guestRepository = new GuestRepository(mockWrapper.Object);
            BookRoomRepository bookRoomRepo = new BookRoomRepository(mockWrapper.Object, guestRepository);
            RoomsRepository roomsRepository = new RoomsRepository(mockWrapper.Object, bookRoomRepo);
            var mockLogger = new Mock<ILogger<BookingController>>();

            var controller = new BookingController(mockLogger.Object);

            //Act
            var result = controller.Get(roomsRepository);
            var okResult = result as OkObjectResult;

            //Assert
            List<RoomDto> returnObject = okResult.Value as List<RoomDto>;
            Assert.AreEqual(200, okResult.StatusCode);
            Assert.IsAssignableFrom<List<RoomDto>>(okResult.Value);
            Assert.IsNotNull(returnObject);

            Assert.AreEqual(1, returnObject.Count);
            Assert.AreEqual(4, returnObject[0].Amenities.Count);
            Assert.AreEqual(2, returnObject[0].BookedDates.Count);
            mockWrapper.VerifyAll();
        }

        [Test]
        public void GetAllBookedDatesThrowsException()
        {
            //Arrange
            var mockWrapper = new Mock<IDBWrapper>();
            mockWrapper.Setup(wrapper => wrapper.GetRooms()).Throws(new Exception("Test Exception"));

            GuestRepository guestRepository = new GuestRepository(mockWrapper.Object);
            BookRoomRepository bookRoomRepo = new BookRoomRepository(mockWrapper.Object, guestRepository);
            RoomsRepository roomsRepository = new RoomsRepository(mockWrapper.Object, bookRoomRepo);
            var mockLogger = new Mock<ILogger<BookingController>>();

            var controller = new BookingController(mockLogger.Object);

            //Act
            var result = controller.Get(roomsRepository);
            var badResult = result as BadRequestObjectResult;
            //Assert
            Assert.AreEqual(400, badResult.StatusCode);
            Assert.AreEqual("Unexpect error getting available dates for rooms, error: Test Exception", badResult.Value);
        }

        [Test]
        public void GetAllBookedDatesDoesNotHaveAmenities()
        {
            //Arrange
            var mockWrapper = new Mock<IDBWrapper>();
            mockWrapper.Setup(wrapper => wrapper.GetRooms()).Returns(TestRooms()).Verifiable();
            mockWrapper.Setup(wrapper => wrapper.GetBooking()).Returns(TestBooking()).Verifiable();

            GuestRepository guestRepository = new GuestRepository(mockWrapper.Object);
            BookRoomRepository bookRoomRepo = new BookRoomRepository(mockWrapper.Object, guestRepository);
            RoomsRepository roomsRepository = new RoomsRepository(mockWrapper.Object, bookRoomRepo);
            var mockLogger = new Mock<ILogger<BookingController>>();

            var controller = new BookingController(mockLogger.Object);

            //Act
            var result = controller.Get(roomsRepository);
            var okResult = result as OkObjectResult;

            //Assert
            List<RoomDto> returnObject = okResult.Value as List<RoomDto>;
            Assert.AreEqual(200, okResult.StatusCode);
            Assert.IsAssignableFrom<List<RoomDto>>(okResult.Value);
            Assert.IsNotNull(returnObject);

            Assert.AreEqual(1, returnObject.Count);
            Assert.IsNull(returnObject[0].Amenities);
            Assert.AreEqual(2, returnObject[0].BookedDates.Count);
            mockWrapper.VerifyAll();
        }

        [Test]
        public void BookingRoom_EndDateIsSoonerThanStartDate()
        {
            //Arrange
            var mockWrapper = new Mock<IDBWrapper>();
            var mockLogger = new Mock<ILogger<BookingController>>();

            GuestRepository guestRepository = new GuestRepository(mockWrapper.Object);
            BookRoomRepository bookRoomRepo = new BookRoomRepository(mockWrapper.Object, guestRepository);

            var controller = new BookingController(mockLogger.Object);

            BookRoomDto bookRoomDto = new BookRoomDto()
            {
                ReservationId = 1,
                ReservationName = "Test Reservation",
                RoomId = 1,
                BookedDates = new BookedDatesDto()
                {
                    StartDate = DateTime.Today.AddDays(2),
                    EndDate = DateTime.Today
                }
            };

            //Act
            var result = controller.Book(bookRoomDto, bookRoomRepo);
            var badResult = result as BadRequestObjectResult;

            //Assert
            Assert.AreEqual(400, badResult.StatusCode);
            Assert.AreEqual($"End date - {DateTime.Today} should be sooner than Start date: {DateTime.Today.AddDays(2)}", badResult.Value);
        }

        [Test]
        public void BookingRoom_StartDateIsTheSameAsToday()
        {
            //Arrange
            var mockWrapper = new Mock<IDBWrapper>();
            var mockLogger = new Mock<ILogger<BookingController>>();

            GuestRepository guestRepository = new GuestRepository(mockWrapper.Object);
            BookRoomRepository bookRoomRepo = new BookRoomRepository(mockWrapper.Object, guestRepository);

            var controller = new BookingController(mockLogger.Object);

            BookRoomDto bookRoomDto = new BookRoomDto()
            {
                ReservationId = 1,
                ReservationName = "Test Reservation",
                RoomId = 1,
                BookedDates = new BookedDatesDto()
                {
                    StartDate = DateTime.Today,
                    EndDate = DateTime.Today.AddDays(1)
                }
            };

            //Act
            var result = controller.Book(bookRoomDto, bookRoomRepo);
            var badResult = result as BadRequestObjectResult;

            //Assert
            Assert.AreEqual(400, badResult.StatusCode);
            Assert.AreEqual($"Start date - {DateTime.Today} must start at least the next current day", badResult.Value);
        }

        [Test]
        public void BookingRoom_IsOver3Days()
        {
            //Arrange
            var mockWrapper = new Mock<IDBWrapper>();
            var mockLogger = new Mock<ILogger<BookingController>>();

            GuestRepository guestRepository = new GuestRepository(mockWrapper.Object);
            BookRoomRepository bookRoomRepo = new BookRoomRepository(mockWrapper.Object, guestRepository);

            var controller = new BookingController(mockLogger.Object);

            BookRoomDto bookRoomDto = new BookRoomDto()
            {
                ReservationId = 1,
                ReservationName = "Test Reservation",
                RoomId = 1,
                BookedDates = new BookedDatesDto()
                {
                    StartDate = DateTime.Today.AddDays(1),
                    EndDate = DateTime.Today.AddDays(5)
                }
            };

            //Act
            var result = controller.Book(bookRoomDto, bookRoomRepo);
            var badResult = result as BadRequestObjectResult;

            //Assert
            Assert.AreEqual(400, badResult.StatusCode);
            Assert.AreEqual($"Cannot book a room for more than 3 days", badResult.Value);
        }

        [Test]
        public void BookingRoom_StartDateIsOver30Days()
        {
            //Arrange
            var mockWrapper = new Mock<IDBWrapper>();
            var mockLogger = new Mock<ILogger<BookingController>>();

            GuestRepository guestRepository = new GuestRepository(mockWrapper.Object);
            BookRoomRepository bookRoomRepo = new BookRoomRepository(mockWrapper.Object, guestRepository);

            var controller = new BookingController(mockLogger.Object);

            BookRoomDto bookRoomDto = new BookRoomDto()
            {
                ReservationId = 1,
                ReservationName = "Test Reservation",
                RoomId = 1,
                BookedDates = new BookedDatesDto()
                {
                    StartDate = DateTime.Today.AddDays(31),
                    EndDate = DateTime.Today.AddDays(32)
                }
            };

            //Act
            var result = controller.Book(bookRoomDto, bookRoomRepo);
            var badResult = result as BadRequestObjectResult;

            //Assert
            Assert.AreEqual(400, badResult.StatusCode);
            Assert.AreEqual($"Cannot book a room more than 30 days in advance", badResult.Value);
        }

        [Test]
        public void BookingRoom_SuccessfulBooking()
        {
            //Arrange
            var mockWrapper = new Mock<IDBWrapper>();
            mockWrapper.Setup(wrapper => wrapper.GetBooking()).Returns(TestBooking()).Verifiable();
            var mockLogger = new Mock<ILogger<BookingController>>();

            GuestRepository guestRepository = new GuestRepository(mockWrapper.Object);
            BookRoomRepository bookRoomRepo = new BookRoomRepository(mockWrapper.Object, guestRepository);

            var controller = new BookingController(mockLogger.Object);

            BookRoomDto bookRoomDto = new BookRoomDto()
            {
                ReservationName = "Test Reservation",
                RoomId = 1,
                BookedDates = new BookedDatesDto()
                {
                    StartDate = DateTime.Today.AddDays(10),
                    EndDate = DateTime.Today.AddDays(12)
                },
                Guests = new List<GuestDto>()
                {
                    new GuestDto()
                    {
                        Document = "123456789",
                        Name = "Test Guest 1",
                        AgeGroup = AgeGroup.Children
                    }
                }
            };

            //Act
            var result = controller.Book(bookRoomDto, bookRoomRepo);
            var okResult = result as OkObjectResult;

            //Assert
            Assert.AreEqual(200, okResult.StatusCode);
        }

        [Test]
        public void ModifyBooking_BookedDate()
        {
            //Arrange
            var mockWrapper = new Mock<IDBWrapper>();
            mockWrapper.Setup(wrapper => wrapper.GetBooking()).Returns(TestBooking()).Verifiable();
            var mockLogger = new Mock<ILogger<BookingController>>();

            GuestRepository guestRepository = new GuestRepository(mockWrapper.Object);
            BookRoomRepository bookRoomRepo = new BookRoomRepository(mockWrapper.Object, guestRepository);

            var controller = new BookingController(mockLogger.Object);

            BookRoomDto bookRoomDto = new BookRoomDto()
            {
                ReservationId = 1,
                ReservationName = "Test Reservation",
                RoomId = 1,
                BookedDates = new BookedDatesDto()
                {
                    StartDate = DateTime.Today.AddDays(10),
                    EndDate = DateTime.Today.AddDays(12)
                },
                Guests = new List<GuestDto>()
                {
                    new GuestDto()
                    {
                        Document = "123456789",
                        Name = "Test Guest 1",
                        AgeGroup = AgeGroup.Children
                    }
                }
            };

            //Act
            var result = controller.Modify(bookRoomDto, bookRoomRepo);
            var okResult = result as OkObjectResult;

            //Assert
            Assert.AreEqual(200, okResult.StatusCode);
        }

        [Test]
        public void ModifyBooking_AlreadyBookedDate()
        {
            //Arrange
            var mockWrapper = new Mock<IDBWrapper>();
            mockWrapper.Setup(wrapper => wrapper.GetBooking()).Returns(TestBooking()).Verifiable();
            var mockLogger = new Mock<ILogger<BookingController>>();

            GuestRepository guestRepository = new GuestRepository(mockWrapper.Object);
            BookRoomRepository bookRoomRepo = new BookRoomRepository(mockWrapper.Object, guestRepository);

            var controller = new BookingController(mockLogger.Object);

            BookRoomDto bookRoomDto = new BookRoomDto()
            {
                ReservationId = 1,
                ReservationName = "Test Reservation",
                RoomId = 1,
                BookedDates = new BookedDatesDto()
                {
                    StartDate = DateTime.Today.AddDays(3),
                    EndDate = DateTime.Today.AddDays(4)
                },
                Guests = new List<GuestDto>()
                {
                    new GuestDto()
                    {
                        Document = "123456789",
                        Name = "Test Guest 1",
                        AgeGroup = AgeGroup.Children
                    }
                }
            };

            //Act
            var result = controller.Modify(bookRoomDto, bookRoomRepo);
            var badResult = result as BadRequestObjectResult;

            //Assert
            Assert.AreEqual(400, badResult.StatusCode);
            Assert.AreEqual($"There's another reservation: {TestBooking()[1]}", badResult.Value);
        }

        [Test]
        public void ModifyBooking_UnidentifiedReservationID()
        {
            //Arrange
            var mockWrapper = new Mock<IDBWrapper>();
            mockWrapper.Setup(wrapper => wrapper.GetBooking()).Returns(TestBooking()).Verifiable();
            var mockLogger = new Mock<ILogger<BookingController>>();

            GuestRepository guestRepository = new GuestRepository(mockWrapper.Object);
            BookRoomRepository bookRoomRepo = new BookRoomRepository(mockWrapper.Object, guestRepository);

            var controller = new BookingController(mockLogger.Object);

            BookRoomDto bookRoomDto = new BookRoomDto()
            {
                ReservationId = 4,
                ReservationName = "Test Reservation",
                RoomId = 1,
                BookedDates = new BookedDatesDto()
                {
                    StartDate = DateTime.Today.AddDays(3),
                    EndDate = DateTime.Today.AddDays(4)
                },
                Guests = new List<GuestDto>()
                {
                    new GuestDto()
                    {
                        Document = "123456789",
                        Name = "Test Guest 1",
                        AgeGroup = AgeGroup.Children
                    }
                }
            };

            //Act
            var result = controller.Modify(bookRoomDto, bookRoomRepo);
            var badResult = result as BadRequestObjectResult;

            //Assert
            Assert.AreEqual(400, badResult.StatusCode);
            Assert.AreEqual($"Reservation ID: {bookRoomDto.ReservationId} not found", badResult.Value);
        }

        [Test]
        public void CancelBooking_AnyID()
        {
            var mockWrapper = new Mock<IDBWrapper>();
            var mockLogger = new Mock<ILogger<BookingController>>();
            GuestRepository guestRepository = new GuestRepository(mockWrapper.Object);
            BookRoomRepository bookRoomRepo = new BookRoomRepository(mockWrapper.Object, guestRepository);

            var controller = new BookingController(mockLogger.Object);

            //Act
            var result = controller.Cancel(1, bookRoomRepo);
            var okResult = result as OkResult;

            //Assert
            Assert.AreEqual(200, okResult.StatusCode);
        }

        [Test]
        public void GetBookings_ValidAmount()
        {
            //Arrange
            var mockWrapper = new Mock<IDBWrapper>();
            mockWrapper.Setup(wrapper => wrapper.GetBooking()).Returns(TestBooking()).Verifiable();
            mockWrapper.Setup(wrapper => wrapper.GetGuests()).Returns(TestGuests()).Verifiable();
            var mockLogger = new Mock<ILogger<BookingController>>();

            GuestRepository guestRepository = new GuestRepository(mockWrapper.Object);
            BookRoomRepository bookRoomRepo = new BookRoomRepository(mockWrapper.Object, guestRepository);

            var controller = new BookingController(mockLogger.Object);

            //Act
            var result = controller.GetBooking(1, bookRoomRepo);
            var okResultObject = result as OkObjectResult;
            BookRoomDto booking = okResultObject.Value as BookRoomDto;

            //Assert
            Assert.AreEqual(200, okResultObject.StatusCode);
            Assert.AreEqual(1, booking.ReservationId);
            Assert.AreEqual(1, booking.Guests.Count);
            Assert.AreEqual(1, booking.Guests[0].GuestID);
            mockWrapper.VerifyAll();
        }

        private List<RoomDto> TestRooms()
        {
            List<RoomDto> output = new List<RoomDto>();

            output.Add(new RoomDto()
            {
                RoomId = 1,
                Name = "Test Room 1",
                Capacity = 4,
                Beds = 2
            });

            return output;
        }

        private List<GuestDto> TestGuests()
        {
            return new List<GuestDto>()
                {
                    new GuestDto()
                    {
                        ReservationID = 1,
                        GuestID = 1,
                        Document = "123456789",
                        Name = "Test Guest 1",
                        AgeGroup = AgeGroup.Children
                    }
                };
        }

        private List<string> TestAmenities()
        {
            return new List<string>() { "Jacuzzi", "Air Conditioning", "Hot Water", "Mini Fridge" };
        }
        private List<BookRoomDto> TestBooking()
        {
            List<BookRoomDto> bookings = new List<BookRoomDto>();

            bookings.Add(new BookRoomDto()
            {
                ReservationId = 1,
                ReservationName = "Test Reservation",
                RoomId = 1,
                BookedDates = new BookedDatesDto()
                {
                    StartDate = DateTime.Today.AddDays(1),
                    EndDate = DateTime.Today.AddDays(2)
                }
            });

            bookings.Add(new BookRoomDto()
            {
                ReservationId = 2,
                ReservationName = "Test Reservation 2",
                RoomId = 1,
                BookedDates = new BookedDatesDto()
                {
                    StartDate = DateTime.Today.AddDays(3),
                    EndDate = DateTime.Today.AddDays(4)
                }
            });

            return bookings;
        }
    }
}
